// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'countries_data.dart';

// **************************************************************************
// IsarCollectionGenerator
// **************************************************************************

// coverage:ignore-file
// ignore_for_file: duplicate_ignore, non_constant_identifier_names, constant_identifier_names, invalid_use_of_protected_member, unnecessary_cast, prefer_const_constructors, lines_longer_than_80_chars, require_trailing_commas, inference_failure_on_function_invocation, unnecessary_parenthesis, unnecessary_raw_strings, unnecessary_null_checks, join_return_with_assignment, prefer_final_locals, avoid_js_rounded_ints, avoid_positional_boolean_parameters, always_specify_types

extension GetCountriesDataCollection on Isar {
  IsarCollection<CountriesData> get countriesDatas => this.collection();
}

const CountriesDataSchema = CollectionSchema(
  name: r'CountriesData',
  id: -7577421391751018718,
  properties: {
    r'alpha2Code': PropertySchema(
      id: 0,
      name: r'alpha2Code',
      type: IsarType.string,
    ),
    r'alpha3Code': PropertySchema(
      id: 1,
      name: r'alpha3Code',
      type: IsarType.string,
    ),
    r'chtName': PropertySchema(
      id: 2,
      name: r'chtName',
      type: IsarType.string,
    ),
    r'engName': PropertySchema(
      id: 3,
      name: r'engName',
      type: IsarType.string,
    ),
    r'numeric': PropertySchema(
      id: 4,
      name: r'numeric',
      type: IsarType.string,
    )
  },
  estimateSize: _countriesDataEstimateSize,
  serialize: _countriesDataSerialize,
  deserialize: _countriesDataDeserialize,
  deserializeProp: _countriesDataDeserializeProp,
  idName: r'id',
  indexes: {
    r'numeric': IndexSchema(
      id: -5269666305100555510,
      name: r'numeric',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'numeric',
          type: IndexType.hash,
          caseSensitive: true,
        )
      ],
    ),
    r'alpha3Code': IndexSchema(
      id: 5425885542379447691,
      name: r'alpha3Code',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'alpha3Code',
          type: IndexType.hash,
          caseSensitive: false,
        )
      ],
    ),
    r'alpha2Code': IndexSchema(
      id: 1052763190871862449,
      name: r'alpha2Code',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'alpha2Code',
          type: IndexType.hash,
          caseSensitive: false,
        )
      ],
    )
  },
  links: {},
  embeddedSchemas: {},
  getId: _countriesDataGetId,
  getLinks: _countriesDataGetLinks,
  attach: _countriesDataAttach,
  version: '3.1.0+1',
);

int _countriesDataEstimateSize(
  CountriesData object,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  var bytesCount = offsets.last;
  {
    final value = object.alpha2Code;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.alpha3Code;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.chtName;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.engName;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.numeric;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  return bytesCount;
}

void _countriesDataSerialize(
  CountriesData object,
  IsarWriter writer,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  writer.writeString(offsets[0], object.alpha2Code);
  writer.writeString(offsets[1], object.alpha3Code);
  writer.writeString(offsets[2], object.chtName);
  writer.writeString(offsets[3], object.engName);
  writer.writeString(offsets[4], object.numeric);
}

CountriesData _countriesDataDeserialize(
  Id id,
  IsarReader reader,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  final object = CountriesData(
    alpha2Code: reader.readStringOrNull(offsets[0]),
    alpha3Code: reader.readStringOrNull(offsets[1]),
    chtName: reader.readStringOrNull(offsets[2]),
    engName: reader.readStringOrNull(offsets[3]),
    id: id,
    numeric: reader.readStringOrNull(offsets[4]),
  );
  return object;
}

P _countriesDataDeserializeProp<P>(
  IsarReader reader,
  int propertyId,
  int offset,
  Map<Type, List<int>> allOffsets,
) {
  switch (propertyId) {
    case 0:
      return (reader.readStringOrNull(offset)) as P;
    case 1:
      return (reader.readStringOrNull(offset)) as P;
    case 2:
      return (reader.readStringOrNull(offset)) as P;
    case 3:
      return (reader.readStringOrNull(offset)) as P;
    case 4:
      return (reader.readStringOrNull(offset)) as P;
    default:
      throw IsarError('Unknown property with id $propertyId');
  }
}

Id _countriesDataGetId(CountriesData object) {
  return object.id ?? Isar.autoIncrement;
}

List<IsarLinkBase<dynamic>> _countriesDataGetLinks(CountriesData object) {
  return [];
}

void _countriesDataAttach(
    IsarCollection<dynamic> col, Id id, CountriesData object) {
  object.id = id;
}

extension CountriesDataQueryWhereSort
    on QueryBuilder<CountriesData, CountriesData, QWhere> {
  QueryBuilder<CountriesData, CountriesData, QAfterWhere> anyId() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(const IdWhereClause.any());
    });
  }
}

extension CountriesDataQueryWhere
    on QueryBuilder<CountriesData, CountriesData, QWhereClause> {
  QueryBuilder<CountriesData, CountriesData, QAfterWhereClause> idEqualTo(
      Id id) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: id,
        upper: id,
      ));
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterWhereClause> idNotEqualTo(
      Id id) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            )
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            );
      } else {
        return query
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            )
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            );
      }
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterWhereClause> idGreaterThan(
      Id id,
      {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.greaterThan(lower: id, includeLower: include),
      );
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterWhereClause> idLessThan(
      Id id,
      {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.lessThan(upper: id, includeUpper: include),
      );
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterWhereClause> idBetween(
    Id lowerId,
    Id upperId, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: lowerId,
        includeLower: includeLower,
        upper: upperId,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterWhereClause>
      numericIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'numeric',
        value: [null],
      ));
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterWhereClause>
      numericIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'numeric',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterWhereClause> numericEqualTo(
      String? numeric) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'numeric',
        value: [numeric],
      ));
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterWhereClause>
      numericNotEqualTo(String? numeric) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'numeric',
              lower: [],
              upper: [numeric],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'numeric',
              lower: [numeric],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'numeric',
              lower: [numeric],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'numeric',
              lower: [],
              upper: [numeric],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterWhereClause>
      alpha3CodeIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'alpha3Code',
        value: [null],
      ));
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterWhereClause>
      alpha3CodeIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'alpha3Code',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterWhereClause>
      alpha3CodeEqualTo(String? alpha3Code) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'alpha3Code',
        value: [alpha3Code],
      ));
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterWhereClause>
      alpha3CodeNotEqualTo(String? alpha3Code) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'alpha3Code',
              lower: [],
              upper: [alpha3Code],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'alpha3Code',
              lower: [alpha3Code],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'alpha3Code',
              lower: [alpha3Code],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'alpha3Code',
              lower: [],
              upper: [alpha3Code],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterWhereClause>
      alpha2CodeIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'alpha2Code',
        value: [null],
      ));
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterWhereClause>
      alpha2CodeIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'alpha2Code',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterWhereClause>
      alpha2CodeEqualTo(String? alpha2Code) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'alpha2Code',
        value: [alpha2Code],
      ));
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterWhereClause>
      alpha2CodeNotEqualTo(String? alpha2Code) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'alpha2Code',
              lower: [],
              upper: [alpha2Code],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'alpha2Code',
              lower: [alpha2Code],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'alpha2Code',
              lower: [alpha2Code],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'alpha2Code',
              lower: [],
              upper: [alpha2Code],
              includeUpper: false,
            ));
      }
    });
  }
}

extension CountriesDataQueryFilter
    on QueryBuilder<CountriesData, CountriesData, QFilterCondition> {
  QueryBuilder<CountriesData, CountriesData, QAfterFilterCondition>
      alpha2CodeIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'alpha2Code',
      ));
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterFilterCondition>
      alpha2CodeIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'alpha2Code',
      ));
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterFilterCondition>
      alpha2CodeEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'alpha2Code',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterFilterCondition>
      alpha2CodeGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'alpha2Code',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterFilterCondition>
      alpha2CodeLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'alpha2Code',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterFilterCondition>
      alpha2CodeBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'alpha2Code',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterFilterCondition>
      alpha2CodeStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'alpha2Code',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterFilterCondition>
      alpha2CodeEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'alpha2Code',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterFilterCondition>
      alpha2CodeContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'alpha2Code',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterFilterCondition>
      alpha2CodeMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'alpha2Code',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterFilterCondition>
      alpha2CodeIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'alpha2Code',
        value: '',
      ));
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterFilterCondition>
      alpha2CodeIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'alpha2Code',
        value: '',
      ));
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterFilterCondition>
      alpha3CodeIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'alpha3Code',
      ));
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterFilterCondition>
      alpha3CodeIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'alpha3Code',
      ));
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterFilterCondition>
      alpha3CodeEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'alpha3Code',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterFilterCondition>
      alpha3CodeGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'alpha3Code',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterFilterCondition>
      alpha3CodeLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'alpha3Code',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterFilterCondition>
      alpha3CodeBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'alpha3Code',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterFilterCondition>
      alpha3CodeStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'alpha3Code',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterFilterCondition>
      alpha3CodeEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'alpha3Code',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterFilterCondition>
      alpha3CodeContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'alpha3Code',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterFilterCondition>
      alpha3CodeMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'alpha3Code',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterFilterCondition>
      alpha3CodeIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'alpha3Code',
        value: '',
      ));
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterFilterCondition>
      alpha3CodeIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'alpha3Code',
        value: '',
      ));
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterFilterCondition>
      chtNameIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'chtName',
      ));
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterFilterCondition>
      chtNameIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'chtName',
      ));
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterFilterCondition>
      chtNameEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'chtName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterFilterCondition>
      chtNameGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'chtName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterFilterCondition>
      chtNameLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'chtName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterFilterCondition>
      chtNameBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'chtName',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterFilterCondition>
      chtNameStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'chtName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterFilterCondition>
      chtNameEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'chtName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterFilterCondition>
      chtNameContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'chtName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterFilterCondition>
      chtNameMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'chtName',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterFilterCondition>
      chtNameIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'chtName',
        value: '',
      ));
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterFilterCondition>
      chtNameIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'chtName',
        value: '',
      ));
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterFilterCondition>
      engNameIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'engName',
      ));
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterFilterCondition>
      engNameIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'engName',
      ));
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterFilterCondition>
      engNameEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'engName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterFilterCondition>
      engNameGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'engName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterFilterCondition>
      engNameLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'engName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterFilterCondition>
      engNameBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'engName',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterFilterCondition>
      engNameStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'engName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterFilterCondition>
      engNameEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'engName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterFilterCondition>
      engNameContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'engName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterFilterCondition>
      engNameMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'engName',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterFilterCondition>
      engNameIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'engName',
        value: '',
      ));
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterFilterCondition>
      engNameIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'engName',
        value: '',
      ));
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterFilterCondition> idIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'id',
      ));
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterFilterCondition>
      idIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'id',
      ));
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterFilterCondition> idEqualTo(
      Id? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterFilterCondition>
      idGreaterThan(
    Id? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterFilterCondition> idLessThan(
    Id? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterFilterCondition> idBetween(
    Id? lower,
    Id? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'id',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterFilterCondition>
      numericIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'numeric',
      ));
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterFilterCondition>
      numericIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'numeric',
      ));
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterFilterCondition>
      numericEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'numeric',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterFilterCondition>
      numericGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'numeric',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterFilterCondition>
      numericLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'numeric',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterFilterCondition>
      numericBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'numeric',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterFilterCondition>
      numericStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'numeric',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterFilterCondition>
      numericEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'numeric',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterFilterCondition>
      numericContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'numeric',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterFilterCondition>
      numericMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'numeric',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterFilterCondition>
      numericIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'numeric',
        value: '',
      ));
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterFilterCondition>
      numericIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'numeric',
        value: '',
      ));
    });
  }
}

extension CountriesDataQueryObject
    on QueryBuilder<CountriesData, CountriesData, QFilterCondition> {}

extension CountriesDataQueryLinks
    on QueryBuilder<CountriesData, CountriesData, QFilterCondition> {}

extension CountriesDataQuerySortBy
    on QueryBuilder<CountriesData, CountriesData, QSortBy> {
  QueryBuilder<CountriesData, CountriesData, QAfterSortBy> sortByAlpha2Code() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'alpha2Code', Sort.asc);
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterSortBy>
      sortByAlpha2CodeDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'alpha2Code', Sort.desc);
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterSortBy> sortByAlpha3Code() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'alpha3Code', Sort.asc);
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterSortBy>
      sortByAlpha3CodeDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'alpha3Code', Sort.desc);
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterSortBy> sortByChtName() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'chtName', Sort.asc);
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterSortBy> sortByChtNameDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'chtName', Sort.desc);
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterSortBy> sortByEngName() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'engName', Sort.asc);
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterSortBy> sortByEngNameDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'engName', Sort.desc);
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterSortBy> sortByNumeric() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'numeric', Sort.asc);
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterSortBy> sortByNumericDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'numeric', Sort.desc);
    });
  }
}

extension CountriesDataQuerySortThenBy
    on QueryBuilder<CountriesData, CountriesData, QSortThenBy> {
  QueryBuilder<CountriesData, CountriesData, QAfterSortBy> thenByAlpha2Code() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'alpha2Code', Sort.asc);
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterSortBy>
      thenByAlpha2CodeDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'alpha2Code', Sort.desc);
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterSortBy> thenByAlpha3Code() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'alpha3Code', Sort.asc);
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterSortBy>
      thenByAlpha3CodeDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'alpha3Code', Sort.desc);
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterSortBy> thenByChtName() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'chtName', Sort.asc);
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterSortBy> thenByChtNameDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'chtName', Sort.desc);
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterSortBy> thenByEngName() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'engName', Sort.asc);
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterSortBy> thenByEngNameDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'engName', Sort.desc);
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterSortBy> thenById() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.asc);
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterSortBy> thenByIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.desc);
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterSortBy> thenByNumeric() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'numeric', Sort.asc);
    });
  }

  QueryBuilder<CountriesData, CountriesData, QAfterSortBy> thenByNumericDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'numeric', Sort.desc);
    });
  }
}

extension CountriesDataQueryWhereDistinct
    on QueryBuilder<CountriesData, CountriesData, QDistinct> {
  QueryBuilder<CountriesData, CountriesData, QDistinct> distinctByAlpha2Code(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'alpha2Code', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<CountriesData, CountriesData, QDistinct> distinctByAlpha3Code(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'alpha3Code', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<CountriesData, CountriesData, QDistinct> distinctByChtName(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'chtName', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<CountriesData, CountriesData, QDistinct> distinctByEngName(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'engName', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<CountriesData, CountriesData, QDistinct> distinctByNumeric(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'numeric', caseSensitive: caseSensitive);
    });
  }
}

extension CountriesDataQueryProperty
    on QueryBuilder<CountriesData, CountriesData, QQueryProperty> {
  QueryBuilder<CountriesData, int, QQueryOperations> idProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'id');
    });
  }

  QueryBuilder<CountriesData, String?, QQueryOperations> alpha2CodeProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'alpha2Code');
    });
  }

  QueryBuilder<CountriesData, String?, QQueryOperations> alpha3CodeProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'alpha3Code');
    });
  }

  QueryBuilder<CountriesData, String?, QQueryOperations> chtNameProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'chtName');
    });
  }

  QueryBuilder<CountriesData, String?, QQueryOperations> engNameProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'engName');
    });
  }

  QueryBuilder<CountriesData, String?, QQueryOperations> numericProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'numeric');
    });
  }
}
